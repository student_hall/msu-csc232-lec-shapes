#include "Rectangle.h"

Rectangle::Rectangle(length_unit length, length_unit width) throw(ShapeParameterException) {
    setLength(length);
    setWidth(width);
}

// getter and setter
length_unit Rectangle::getLength() const {
    return m_length;
}

void Rectangle::setLength(const length_unit length) throw(ShapeParameterException) {
    if (length <= 0) {
        throw ShapeParameterException("length cannot be less than or equal to zero.");
    }
    m_length = length;
}

length_unit Rectangle::getWidth() const {
    return m_width;
}

void Rectangle::setWidth(const length_unit width) throw(ShapeParameterException) {
    if (width <= 0) {
        throw ShapeParameterException("width cannot be less than or equal to zero.");
    }
    m_width = width;
}

// override
length_unit Rectangle::getPerimeter() const {
    return 2 * (getLength() + getWidth());
}

area_unit Rectangle::getArea() const {
    return getLength() * getWidth();
}

std::string Rectangle::getName() const {
    return "Rectangle";
}

Rectangle operator +(const Rectangle& lhs, const Rectangle& rhs) {
    length_unit width{lhs.getWidth() + rhs.getWidth()};
    length_unit length{lhs.getLength() + rhs.getLength()};
    //Rectangle sum(length, width);
    return Rectangle{length, width};
}

bool operator ==(const Rectangle& lhs, const Rectangle& rhs) {
    return ((lhs.getLength() == rhs.getLength()) && (lhs.getWidth() == rhs.getWidth()));
}

bool operator !=(const Rectangle& lhs, const Rectangle& rhs) {
    return !(lhs == rhs);
}

std::istream& operator >>(std::istream& ins, Rectangle& target) {
    ins >> target.m_length >> target.m_width; // This function must be a friend to have this kind of direct access
    return ins;
}

std::ostream& operator <<(std::ostream& outs, const Rectangle& source) {
    outs << "[length = " << source.getLength() << ", width = " << source.getWidth() << "]";
    return outs;
}