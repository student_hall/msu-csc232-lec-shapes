#include <string>

#ifndef __SHAPE_H__
#define __SHAPE_H__

using length_unit = double;
using area_unit   = double;
using volume_unit = double;

class Shape {
public:
    virtual std::string getName() const = 0;
};

#endif // __SHAPE_H__
