#include <cstdlib>
#include <iomanip>
#include <iostream>

#include "Shape.h"
#include "TwoDimensionalShape.h"
#include "Rectangle.h"
#include "Square.h"
#include "ShapeParameterException.h"

using array_index = int;

void printRectangleInfo(const Rectangle& r);
TwoDimensionalShape* findShapeWithLargestArea(TwoDimensionalShape* shapes[], 
    array_index first, array_index last);
TwoDimensionalShape* max(TwoDimensionalShape* left, 
    TwoDimensionalShape* right);

int main(int argc, char** argv) {
    length_unit length{4.0};
    length_unit width{2.0};
    Rectangle* r1 = new Rectangle{length, width};
    
    printRectangleInfo(*r1);

    try {
        r1->setLength(2 * length);
        r1->setWidth(-2 * width);
    } catch (const ShapeParameterException& spe) {
        // report error
        std::cerr << spe.what() << std::endl;
        // fix the error
        r1->setWidth(2 * width);
    } // and move on...
    

    printRectangleInfo(*r1);

    Square* s1 = new Square{3};
    printRectangleInfo(*s1);

    Rectangle* s2 = new Square{2.5};
    printRectangleInfo(*s2);

    // TwoDimensionalShapes ...
    TwoDimensionalShape* myShapes[] = {r1, s1, s2};

    TwoDimensionalShape* largest = findShapeWithLargestArea(myShapes, 0, 2);
    std::cout << "Shape: " << largest->getName() << " with area = " 
              << largest->getArea() << std::endl;

    // Input a Rectangle
    Rectangle rectangle;
    std::cout << "Enter length and width: ";
    std::cin >> rectangle;
    std::cout << rectangle;

    // Output a Rectangle
    std::cout << std::endl << "Output *r1" << std::endl;
    std::cout << *r1 << std::endl;

    // Compare Rectangle objects
    if (rectangle == rectangle) {
        std::cout << "Naturally, rectangle == rectangle" << std::endl;
    }

    if (s1 != largest) {
        std::cout << "Hey look! The square isn't the largest two-dimensional shape!" << std::endl;
        std::cout << "But isn't it a cool thing that I can compare them!" << std::endl;
    }

    // Let's create another one by adding them together
    Rectangle bigRectangle = rectangle + *r1;
    std::cout << "bigRectangle = rectangle + *r1 = " << bigRectangle << std::endl;

    return EXIT_SUCCESS;
}

void printRectangleInfo(const Rectangle& r) {
    std::cout << std::setw(10) << std::left << "name:";
    std::cout << std::setw(10) << std::right << r.getName() << "\n";
    std::cout << std::setw(10) << std::left << "length:";
    std::cout << std::setw(10) << std::right << std::fixed
              << std::setprecision(2) << r.getLength()  << "\n";
    std::cout << std::setw(10) << std::left << "width:";
    std::cout << std::setw(10) << std::right << std::fixed
              << std::setprecision(2) << r.getWidth() << "\n";
    std::cout << std::setw(10) << std::left << "perimeter:";
    std::cout << std::setw(10) << std::right << std::fixed
              << std::setprecision(2) << r.getPerimeter() << "\n";
    std::cout << std::setw(10) << std::left << "area:";
    std::cout << std::setw(10) << std::right << std::fixed
              << std::setprecision(2) << r.getArea() << std::endl 
              << std::endl;
}

TwoDimensionalShape* findShapeWithLargestArea(TwoDimensionalShape* shapes[], 
    array_index first, array_index last) {
    if (first == last) {
        return shapes[first];
    } else {
        array_index mid = first + (last - first) / 2;
        return max(findShapeWithLargestArea(shapes, first, mid), 
                   findShapeWithLargestArea(shapes, mid + 1, last));
    }
}

TwoDimensionalShape* max(TwoDimensionalShape* left, TwoDimensionalShape* right) {
    return (left->getArea() > right->getArea() ? left : right);
}
