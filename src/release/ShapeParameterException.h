#ifndef SHAPE_PARAMETER_EXCEPTION_H__
#define SHAPE_PARAMETER_EXCEPTION_H__

#include <stdexcept>
#include <string>

/**
 * A wrapper around the logic error class that provides semantic value in the
 * Shapes hierarchy.
 */
class ShapeParameterException : public std::invalid_argument {
public:
    ShapeParameterException(const std::string& message = "");
};

#endif // SHAPE_PARAMETER_EXCEPTION_H__
